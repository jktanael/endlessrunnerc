// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacter2.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/FloatingPawnMovement.h"

// Sets default values
ARunCharacter2::ARunCharacter2()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Capsule = GetCapsuleComponent();
	SpringArm = CreateDefaultSubobject<USpringArmComponent>("Spring Arm");
	SpringArm->SetupAttachment(Capsule);
	SpringArm->TargetArmLength = 500.0f;
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(SpringArm);
	Movement = CreateDefaultSubobject<UFloatingPawnMovement>("Movement");
}

// Called when the game starts or when spawned
void ARunCharacter2::BeginPlay()
{
	Super::BeginPlay();
	
}

void ARunCharacter2::MoveForward(float scale)
{

	Movement->AddInputVector(GetActorForwardVector() * scale);
}

void ARunCharacter2::MoveRight(float scale)
{
	Movement->AddInputVector(GetActorRightVector() * scale);
}


// Called every frame
void ARunCharacter2::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}



// Called to bind functionality to input
//void ARunCharacter2::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
//{
//	Super::SetupPlayerInputComponent(PlayerInputComponent);
//
//}
