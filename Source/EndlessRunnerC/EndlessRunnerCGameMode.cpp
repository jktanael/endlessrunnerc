// Copyright Epic Games, Inc. All Rights Reserved.

#include "EndlessRunnerCGameMode.h"
#include "EndlessRunnerCCharacter.h"
#include "UObject/ConstructorHelpers.h"

AEndlessRunnerCGameMode::AEndlessRunnerCGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
