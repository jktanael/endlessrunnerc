// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "TimerManager.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("Scene Component");
	SetRootComponent(SceneComponent);
	AttachPoint = CreateDefaultSubobject<UArrowComponent>("Attach Point");
	AttachPoint->SetupAttachment(SceneComponent);
	ExitTrigger = CreateDefaultSubobject<UBoxComponent>("Exit Trigger");
	ExitTrigger->SetupAttachment(SceneComponent);
	hasCreatedTile = false;
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();

	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnTriggerTouched);
	//ExitTrigger->OnComponentHit.AddDynamic(this, &ATile::OnTriggerTouched);
	//currentAttachVector = AttachPoint->GetComponentTransform();

}

void ATile::DestroyTile()
{
	this->Destroy();
}

void ATile::DestroyTileWithDelay(float delay)
{
	FTimerHandle TimerHandle;
	FTimerDelegate DestroyDelegate = FTimerDelegate::CreateUObject(this, &ATile::DestroyTile);
	GetWorldTimerManager().SetTimer(TimerHandle, DestroyDelegate, delay, false);
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATile::OnTriggerTouched(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("test"));
	if (!hasCreatedTile)
		AddTile();
	DestroyTileWithDelay(5);
}

void ATile::AddTile()
{
	if (!TileClass) return;
	hasCreatedTile = true;
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;
	//spawnParams.bNoFail = true;
	spawnParams.Owner = this;

	FTransform transform;
	transform.SetLocation(AttachPoint->GetComponentTransform().GetLocation() + FVector(10.0f, 0, 0));
	transform.SetRotation(GetActorRotation().Quaternion());
	transform.SetScale3D(FVector(1.0f));

	GetWorld()->SpawnActor<ATile>(TileClass, transform, spawnParams);
	//newTile->currentAttachVector = newTile->AttachPoint->GetRelativeLocation();

}

