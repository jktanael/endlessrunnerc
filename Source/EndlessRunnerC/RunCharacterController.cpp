// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "RunCharacter2.h"
#include "GameFramework/FloatingPawnMovement.h"

ARunCharacterController::ARunCharacterController() 
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

}

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();

	RunCharacter = Cast<ARunCharacter2>(GetPawn());
}

void ARunCharacterController::MoveForward(float scale)
{
	
	//Movement->AddInputVector(GetActorForwardVector() * scale);
}

void ARunCharacterController::MoveRight(float scale)
{
	RunCharacter->MoveRight(scale);
}

// Called every frame
void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	RunCharacter->MoveForward(1);

}

// Called to bind functionality to input
void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this , &ARunCharacterController::MoveRight);
}